/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import PropsExample from './src/PropsExample';
import BlinkApp from './src/StateExample';
import ApplyStyles from './src/StyleExample';
import DimensionsBasic from './src/SizeExample';
import FlexDimensionsBasic from './src/FlexSizeExample';
import FlexboxExample from './src/FlexboxExample';
import HandlingTextInput from './src/HandleTextExample';
import HandleTouch from './src/HandleTouch';
import ScrollExample from './src/ScrollExample';
import ListExample from './src/ListExample';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ListExample);
