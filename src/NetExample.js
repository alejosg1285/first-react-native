import React from 'react';
import { FlatList, ActivityIndicator, Text, View } from 'react-native';
import console = require('console');

export default class NetExaple extends React.Component {
    constructor(props) {
        super(props);

        this.state = { isLoading: true };
    }

    /**
     * Use async - await to make a http request.
     */
    async getMovies() {
        try {
            let response =  await fetch('https://facebook.github.io/react-native/movies.json');
            let respJson = await response.json();
            
            return respJson;
        } catch(error) {
            console.log(error);
        }
    }

    componentDidMount() {
        // Use a regular fetch sintaxt to make a http request.
        /*return fetch('https://facebook.github.io/react-native/movies.json')
        .then(response => response.json())
        .then(respJson => {
            this.setState({
                isLoading: false,
                dataSource: respJson.movies
            }, function() {});
        })
        .catch(error => console.log(error));*/

        let response = getMovies();
        this.setState({
            isLoading: false,
            dataSource: response.movies
        }
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            );
        }

        return (
            <View style={{flex: 1, paddingTop: 20}}>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({item}) => <Text>{item.title}, {item.releaseYear}</Text>}
                    keyExtractor={({id}, index) => id}
                />
            </View>
        );
    }
}