import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';

class Blink extends Component {
  constructor(props) {
      super(props);

      // state que permite controlar si mostrar o no el texto.
      this.state = { isShowingText: true };
  
      // Cada segundo se cambia el valor del state
      setInterval(() => (
        this.setState(previousState => (
          { isShowingText: !previousState.isShowingText }
        ))
      ), 1000);
  }

  render() {
    // Si el valor del state es false, retorna un nulo.
    if (!this.state.isShowingText) {
      return null;
    }

    return (
      <Text>{this.props.text}</Text>
    );
  }
}

export default class BlinkApp extends Component {
  // Invocar el componente Blick y enviarle un valor al state text.
  render() {
    return (
      <View>
          <Blink text='Ahora me ves' />
          <Blink text='Ahora no me ves' />
      </View>
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => BlinkApp);