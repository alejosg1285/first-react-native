import React, { Component } from 'react';
import { AppRegistry, Image, Text, View } from 'react-native';

class SayHi extends Component {
    render() {
        return (
            <View style={{alignItems: 'center'}}>
                <Text>Hallo {this.props.name}!</Text>
            </View>
        );
    }
}

export default class PropsExample extends Component {
    static navigationOptions = { title: 'Props Example' };

    render() {
        let pic = {
            uri: 'https://www.joblo.com/assets/images/oldsite/images_arrownews/aithrealgodzillafx.jpg'
        };
        // Pasar un enlace al atributo source.
        // Dar valor al prop de un componente.
        return (
            <View>
                <Image source={pic} style={{width: 286, height: 220}} />
                
                <SayHi name="Luna" />
                <SayHi name="Isma" />
            </View>
        );
    }
}

AppRegistry.registerComponent('AwesomeProject', () => PropsExample);