import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';

export default class DimensionsBasic extends Component {
    render() {
        return (
            // Aplicar estilos css en los atributos, no se usar medidas de dimension como px
            <View>
                <View style={{width:50, height: 50, backgroundColor: 'powderblue'}} />
                <View style={{width:100, height: 100, backgroundColor: 'skyblue'}} />
                <View style={{width:150, height: 150, backgroundColor: 'steelblue'}} />
            </View>
        );
    }
}

AppRegistry.registerComponent('AwesomeProject', () => DimensionsBasic);