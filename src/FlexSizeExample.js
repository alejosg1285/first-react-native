import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';

export default class FlexDimensionsBasic extends Component {
    render() {
        return (
            // Al usar flex, el contenedor debe tener dimensiones de tamaño establecidas 
            // con with y height o ser un contenedor flex
            <View style={{flex: 1}}>
                <View style={{flex: 1, backgroundColor: 'powderblue'}} />
                <View style={{flex: 2, backgroundColor: 'skyblue'}} />
                <View style={{flex: 3, backgroundColor: 'steelblue'}} />
            </View>
        );
    }
}

AppRegistry.registerComponent('AwesomeProject', () => FlexDimensionsBasic);