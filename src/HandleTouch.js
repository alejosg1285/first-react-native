import React from 'react';
import { Alert, AppRegistry, Button, StyleSheet, View, Platform, TouchableHighlight, TouchableOpacity, Text } from 'react-native';

export default class HandleTouch extends React.Component {
    _onPressButton() {
        Alert.alert('Button touched');
    }

    _onPressTouchable() {
        Alert.alert('Touchable touches');
    }

    render() {
        // En los botones se define el evento 'onPress' para disparar eventos al pulsar el boton.
        // Tambien se pueden usar los 'Touchable'.
        return (
            <View style={styles.container}>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={this._onPressButton}
                        title="Touch me" />
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={this._onPressButton}
                        title="Press me"
                        color="#841584" />
                </View>
                <View style={styles.alternativeLayoutButtonContainer}>
                    <Button
                        onPress={this._onPressButton}
                        title="Other button" />
                    <Button
                        onPress={this._onPressButton}
                        title="Ok!"
                        color="#841584" />
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableHighlight
                        onPress={this._onPressTouchable}
                        underlayColor="white">
                            <View style={styles.button}>
                                <Text style={styles.buttonText}>Touchable highlight</Text>
                            </View>
                        </TouchableHighlight>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={this._onPressTouchable}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>Touchable opacity</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    buttonContainer: {
        margin: 20
    },
    alternativeLayoutButtonContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    button: {
        marginBottom: 30,
        width: 260,
        alignItems: 'center',
        backgroundColor: '#2196F3'
    },
    buttonText: {
        padding: 20,
        color: 'white'
    }
});

AppRegistry.registerComponent('AwesomeProject', () => HandleTouch);