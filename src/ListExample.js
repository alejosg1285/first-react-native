import React, { Component } from 'react';
import { AppRegistry, FlatList, ScrollView, StyleSheet, Text, View } from 'react-native';

export default class ListExample extends Component {
    render() {
      // 'FlatList' y 'SectionList' funcionan casi de igual manera que el 'ScrollView' excepto que los
      // primeros muestran informacion que esta en pantalla, la diferencia entre 'ListView' y el
      // 'SectionList' es el como despliegan la informacion, el segundo lo despliegue agrupa por regiones.
      return (
          <ScrollView>
        <View style={styles.container}>
          <FlatList
            data={[
              {key: 'Devin'},
              {key: 'Jackson'},
              {key: 'James'},
              {key: 'Joel'},
              {key: 'John'},
              {key: 'Jillian'},
              {key: 'Jimmy'},
              {key: 'Julie'},
            ]}
            renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
          />
          <SectionList
          sections={[
            {title: 'D', data: ['Devin']},
            {title: 'J', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie']},
          ]}
          renderItem={({item}) => <Text style={styles.item}>{item}</Text>}
          renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
          keyExtractor={(item, index) => index}
        />
        </View>
        </ScrollView>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
     flex: 1,
     paddingTop: 22
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
  });

AppRegistry.registerComponent('AwesomeProject', () => ListExample);