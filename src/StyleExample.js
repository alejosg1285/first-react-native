import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View } from 'react-native';

// Definicion de objeto JavaScript con las clases css.
const styles = StyleSheet.create({
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30
    },
    red: {
        color: 'red'
    }
});

export default class ApplyStyles extends Component {
    render() {
        // Usar una o varias clases css en los componentes en la etiqueta style
        return (
            <View>
                <Text style={styles.bigBlue}>Applaying styles</Text>
                <Text style={styles.red}>Applaying other style</Text>
                <Text style={[styles.bigBlue, styles.red]}>Applying multiple styles</Text>
            </View>
        );
    }
}

AppRegistry.registerComponent('AwesomeProject', () => ApplyStyles);