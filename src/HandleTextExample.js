import React, { Component } from 'react';
import { AppRegistry, Text, TextInput, View } from 'react-native';

export default class HandlingTextInput extends Component {
    constructor(props) {
        super(props);
        
        this.state = {text: ''};
    }

    render() {
        // Al igual que en React, se define un metodo 'onChangeText' para capturar los cambios de texto
        // en los componentes TextInput
        return (
            <View style={{padding: 10}}>
                <TextInput
                    style={{height: 40}}
                    placeholder="Type some text"
                    onChangeText={(text) => this.setState({text})}
                />
                <Text style={{padding: 10, fontSize: 42}}>
                    {this.state.text.split(' ').map((word) => word && 'bla').join(' ')}
                </Text>
            </View>
        );
    }
}

AppRegistry.registerComponent('AwesomeProject', () => HandlingTextInput);