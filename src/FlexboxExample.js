import React from 'react';
import { AppRegistry, View } from 'react-native';

export default class FlexboxExample extends React.Component {
  render() {
    return (
      // Se usa de igual manera que el flexbox de html, la unica diferencia es que la propiedad
      // 'flex-direction' es 'column' por defecto
      // La propiedad `alignItems` con el valor 'flex-start' ubica los elementos al inicio del contenedor 
      // la distribuccion es en fila
      // La propiedad `justifyContent` con el valor `flex-end` ubica los elementos al final del contenedor
      // cuando la distribuccion es en columna.
      // La propiedad `flexDirection` con el valor `row` cambia la distribuccion del contenedor,
      // de una columna a una fila.
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
      }}>
        <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
        <View style={{height: 50, backgroundColor: 'skyblue'}} />
        <View style={{height: 100, backgroundColor: 'steelblue'}} />
      </View>
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => FlexboxExample);